package inheritence;

public class Sub extends Super {
	int num=10;
	public void display(){
		System.out.println("This is the display method of sub calss");
	}
	public void mymethod() {
		Sub sub=new Sub();
		sub.display();
		super.display();
		System.out.println(super.num);
		System.out.println(sub.num);
		
	}
	public static void main (String []arg) {
		Sub obj=new Sub();
		obj.mymethod();
		
	}

}
